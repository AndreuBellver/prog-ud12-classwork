package com.bellver.prog.A4;

import com.bellver.prog.A4.Helper.PasswordHelper;
import com.bellver.prog.A4.Repository.DbUserRepository;
import com.bellver.prog.A4.User.User;
import javafx.fxml.FXML;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;


public class LoginController {

    private DbUserRepository dbUserRepository = new DbUserRepository();
    private PasswordHelper passwordHelper = new PasswordHelper();

    @FXML
    private TextField username;

    @FXML
    private PasswordField password;

    @FXML
    private TextField errorMessage;



    @FXML
    private void login(){

        System.out.printf("Username: %s, Password: %s", username.getText(), password.getText());

        dbUserRepository.findAll();

        User user = dbUserRepository.findByEmail(username.getText());

        if (user != null){

            String passwordText = password.getText();
            String passwordSha = user.getPassword();

            if (passwordHelper.isCorrectSha(passwordText,passwordSha)){


                System.out.println("-LOGIN ACEPTADO.");

            } else {

                System.out.println("-PASSWORD INCORRECTA");

            }

        } else {

            System.out.println("EMAIL INCORRECTO");

        }


    }
    @FXML private javafx.scene.control.Button closeButton;

    @FXML
    private void closeButtonAction(){
        Stage stage = (Stage) closeButton.getScene().getWindow();
        stage.close();
    }

    private void showMessage(String message){

    }
}
