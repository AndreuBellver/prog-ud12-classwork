package com.bellver.prog.A4.User;

public class UserNotFoundException extends Exception {

    public UserNotFoundException(){

        super("User not found: Error Id");
    }
}
