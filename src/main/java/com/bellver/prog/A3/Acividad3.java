package com.bellver.prog.A3;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Acividad3 extends Application {

    @Override
    public void start(Stage primaryStage) {

        Board board = new Board(3);
        BoardView boardView = new BoardView(board);
        Scene scene = new Scene(boardView, 300, 350);
        primaryStage.setTitle("Activity1");
        primaryStage.setScene(scene);
        primaryStage.setResizable(false);
        primaryStage.show();

    }

    public static void main(String[] args) {
        launch(args);
    }

}
