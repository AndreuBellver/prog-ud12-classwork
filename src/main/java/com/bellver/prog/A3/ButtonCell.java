package com.bellver.prog.A3;

import javafx.scene.control.Button;

public class ButtonCell extends Button {

    private Coordinate coordinate;

    public ButtonCell(Coordinate coordinate) {

        this.coordinate = coordinate;

    }

    public Coordinate getCoordinate() {
        return coordinate;
    }

}
