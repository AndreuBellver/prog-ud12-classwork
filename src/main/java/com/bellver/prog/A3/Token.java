package com.bellver.prog.A3;

public enum Token {

    NONE {
        @Override
        public String toString() {
            return "";
        }

    }, BLACK {
        @Override
        public String toString() {
            return "X";
        }

    }, WHITE {
        @Override
        public String toString() {
            return "O";
        }
    }

}
