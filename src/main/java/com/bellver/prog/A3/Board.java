package com.bellver.prog.A3;

public class Board {

    private Token statusBoard[][];

    private int dimension;

    private int numPieces;

    private boolean finished;

    public Board(int dimension) {

        this.dimension = dimension;
        statusBoard = new Token[dimension][dimension];
        this.finished = false;

    }

    public boolean setPiece(Coordinate coordinate, Token color){

        if (statusBoard[coordinate.getX()][coordinate.getY()] != null) {
            return false;
        }

        statusBoard[coordinate.getX()][coordinate.getY()] = color;
        numPieces++;

        return true;

    }

    public boolean isTicTacToe(Token color){

        if (isTicTacToeLineal(color) || isTicTacToeDiagonal(color)) {

            this.finished = true;
            return true;

        }

        return false;

    }

    public boolean hasFinished() {

        if (this.finished) {

            return true;

        }

        return numPieces == (dimension * dimension);

    }

    private boolean isTicTacToeLineal(Token color){

        for (int i = 0; i < this.dimension; i++) {

            int numChecksHorizontal = 0;
            int numChecksVertical = 0;

            System.out.println();

            for (int j = 0; j < this.dimension; j++) {

                System.out.print(statusBoard[i][j]);

                if (statusBoard[i][j] == color) {

                    numChecksHorizontal++;

                }

                if (statusBoard[j][i] == color) {

                    numChecksVertical++;

                }

            }

            if (numChecksHorizontal == 3 || numChecksVertical == 3) {

                return true;

            }

        }

        return false;


    }

    private boolean isTicTacToeDiagonal(Token color){

        int numChecksNormal = 0;
        int numChecksInverted = 0;

        for (int i = 0; i < dimension; i++) {

            if (statusBoard[i][i] == color) {

                numChecksNormal++;

            }

            if (statusBoard[dimension-1-i][i] == color) {

                numChecksInverted++;

            }

        }

        return numChecksInverted == 3 || numChecksNormal ==3;

    }

    public int getDimension() {

        return dimension;

    }

}
