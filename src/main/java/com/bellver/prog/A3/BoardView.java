package com.bellver.prog.A3;

import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;

public class BoardView extends GridPane {

    private MyBoardEventHandler boardEventHandler;

    private Board board;

    private Label indicator;

    private Token tokenTurn;

    public BoardView(Board board) {

        this.boardEventHandler = new MyBoardEventHandler();
        this.board = board;
        this.initUi();

    }

    private void initUi() {

        this.indicator = new Label("Bienvenido");
        GridPane.setHalignment(this.indicator, HPos.CENTER);
        GridPane.setColumnSpan(this.indicator, 3);
        this.add(this.indicator, 0, 0);
        initUIbuttons();

    }

    private void initUIbuttons() {

        for (int i = 0; i < board.getDimension(); i++) {

            for (int j = 0; j < board.getDimension(); j++) {

                ButtonCell button = new ButtonCell(new Coordinate(i,j));
                button.setPrefSize(100, 100);
                button.setOnMouseClicked(boardEventHandler);
                this.add(button, i, j+1);

            }

        }

    }

    private Token getPlayerTurn(){

        if (tokenTurn == Token.BLACK) {
            return Token.WHITE;
        }

        return Token.BLACK;

    }

    private void setPiece(ButtonCell button){

        Token turn = getPlayerTurn();

        if (!board.setPiece(button.getCoordinate(), turn)) {

            this.indicator.setText("Ocupada");

        } else {

            this.indicator.setText("");
            this.tokenTurn = turn;
            button.setText(turn.toString());

            if (board.isTicTacToe(turn)) {

                this.indicator.setText( turn + " ha ganado");

            } else if (board.hasFinished()) {

                this.indicator.setText("Empate");

            }

        }


    }

    class MyBoardEventHandler implements EventHandler{

        @Override
        public void handle(Event event) {

            if (board.hasFinished()) {
                return;
            }

            if (event.getSource() instanceof Button) {
                ButtonCell button = (ButtonCell) event.getSource();
                setPiece(button);
            }

        }
    }

}
