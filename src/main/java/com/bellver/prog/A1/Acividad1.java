package com.bellver.prog.A1;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class Acividad1 extends Application {

    @Override
    public void start(Stage primaryStage) {

        Label lblTitle = new Label("Europe/London");
        Label lbLTime = new Label(getEuropeLondonTime());
        VBox vBox = new VBox(lblTitle,lbLTime);
        vBox.setSpacing(15);
        vBox.setAlignment(Pos.CENTER);

        Scene scene = new Scene(vBox, 200, 200);
        primaryStage.setTitle("Activity1");
        primaryStage.setScene(scene);
        primaryStage.setResizable(false);
        primaryStage.show();

    }

    public static void main(String[] args) {
        launch(args);
    }

    private static String getEuropeLondonTime() {

        LocalDateTime nowDateTime = LocalDateTime.now();
        ZonedDateTime converted = nowDateTime.atZone(ZoneId.of("Europe/London"));
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("hh:mm:ss");
        return converted.format(formatter);

    }
}
