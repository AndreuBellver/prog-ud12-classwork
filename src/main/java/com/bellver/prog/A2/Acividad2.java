package com.bellver.prog.A2;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Acividad2 extends Application {

    @Override
    public void start(Stage primaryStage) {

        BoardView board = new BoardView(3,3);
        Scene scene = new Scene(board, 300, 300);
        primaryStage.setTitle("Activity1");
        primaryStage.setScene(scene);
        primaryStage.setResizable(false);
        primaryStage.show();

    }

    public static void main(String[] args) {
        launch(args);
    }

}
