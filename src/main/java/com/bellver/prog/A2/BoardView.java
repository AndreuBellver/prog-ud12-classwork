package com.bellver.prog.A2;

import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;

public class BoardView extends GridPane {

    private MyBoardEventHandler boardEventHandler;

    public BoardView(int numRows, int numColumns) {

        this.boardEventHandler = new MyBoardEventHandler();
        initUIbuttons(numRows,numColumns);

    }

    private void initUIbuttons(int numRows, int numColumns){

        for (int i = 0; i < numRows; i++) {

            for (int j = 0; j < numColumns; j++) {

                Button button = new Button();
                button.setText(i + "," + j);
                button.setPrefSize(100, 100);
                button.setOnMouseClicked(boardEventHandler);
                button.setId(i  + "," + j);
                this.add(button, i, j);

            }

        }

    }

    private void toggleButton(Button button) {

        if (button.getText().equalsIgnoreCase("clicked")){

            button.setText(button.getId());

        }else {

            button.setText("clicked");

        }

    }

    class MyBoardEventHandler implements EventHandler{

        @Override
        public void handle(Event event) {

            if (event.getSource() instanceof Button) {
                Button button = (Button) event.getSource();
                toggleButton(button);
            }

        }
    }

}
